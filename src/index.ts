import {Telegraf} from 'telegraf';
import * as comandos from './comandos.json';

if (process.env.NODE_ENV === 'LOCAL') {
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  require('dotenv').config();
}

const bot = new Telegraf(process.env.TELEGRAM as string);

bot.command('list', ctx => {
  ctx.reply('Available triggers:\n\n' + Object.keys(comandos).join('\n'));
});

type Comando = {
  type: string;
  value: string;
  extraValue: string;
  timer: number;
  UltEjecucion?: Date;
};

function timeringo(comando: Comando): boolean {
  const tActual = new Date();
  console.log('Tactual  ' + tActual.getTime() / 60000);

  if (
    !comando.UltEjecucion ||
    tActual.getTime() - comando.UltEjecucion.getTime() >= comando.timer * 60000
  ) {
    comando.UltEjecucion = tActual;
    console.log('ultima  ' + comando.UltEjecucion?.getTime() / 60000);

    return true;
  }
  return false;
}

const mapaComandos = new Map(Object.entries(comandos));
// Vemos que comando nos han envidado y respondemos
bot.on('text', ctx => {
  const cmd = ctx.message.text.toLowerCase();
  //ctx.reply(cmd);
  mapaComandos.forEach((value: Comando, key) => {
    console.log('Comando ' + key);
    if (cmd.includes(key) && timeringo(value)) {
      switch (value.type) {
        case 'sticker':
          ctx.reply(value.extraValue, {
            reply_to_message_id: ctx.message.message_id,
          });
          ctx.replyWithSticker(value.value);
          break;
        case 'texto':
          ctx.reply(value.value);
          break;
        case 'video':
          ctx.reply(value.extraValue, {
            reply_to_message_id: ctx.message.message_id,
          });
          ctx.replyWithVideo(value.value);
          break;
        default:
          break;
      }
      return;
    }
  });
});

bot.launch();
